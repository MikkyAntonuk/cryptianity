pragma solidity ^0.4.18;

import "zeppelin-solidity/contracts/token/MintableToken.sol";

// Default ERC20 Mintable token for ICO, which can be exchanged to the Soul

contract SoulToken is MintableToken {
    string public constant name = "Soul Token";
    string public constant symbol = "SLT";
    uint8 public constant decimals = 18;
}
