pragma solidity ^0.4.18;

import "./SoulBase.sol";
import "./ERC721.sol";

// Contract that manages ownership of soul
contract SoulsOwnership is SoulBase, ERC721 {

    // Fields declared in ERC721
    string public name = "Soul";
    string public symbol = "SOL";

    function _owns(address _claimant, uint256 _tokenId) internal view returns (bool) {
        return soulIndexToOwner[_tokenId] == _claimant;
    }

    function _approvedFor(address _claimant, uint256 _tokenId) internal view returns (bool) {
        return soulIndexToApproved[_tokenId] == _claimant;
    }

    function _approve(uint256 _tokenId, address _approved) internal {
        soulIndexToApproved[_tokenId] = _approved;
    }

    // Called when soul was accidentally send to bad address. Only admins have permissions to do it
    function rescue(uint256 _soulId, address _recipient) public onlyAdmin whenNotPaused {
        require(_owns(this, _soulId));
        _transfer(this, _recipient, _soulId);
    }

    function balanceOf(address _owner) public view returns (uint256 count) {
        return soulTokenCount[_owner];
    }

    function transferFrom(address _from, address _to, uint256 _tokenId) public whenNotPaused {
        // Check for approval and valid ownership
        require(_approvedFor(msg.sender, _tokenId));
        require(_owns(_from, _tokenId));

        // Reassign ownership (also clears pending approvals and emits Transfer event).
        _transfer(_from, _to, _tokenId);
    }

    function totalSupply() public view returns (uint) {
        return souls.length - 1;
    }

    function ownerOf(uint256 _tokenId) public view returns (address owner) {
        owner = soulIndexToOwner[_tokenId];

        require(owner != address(0));
    }
}
