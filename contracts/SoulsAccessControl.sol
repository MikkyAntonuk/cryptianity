pragma solidity ^0.4.18;

// Contract that manages access privileges

contract SoulsAccessControl {

    // Calls when contract was upgraded and UI should interact with new address
    event ContractUpgrade(address _newContract);

    // developers have permissions to pause and resume work of contract, upgrade contract
    address public developer;
    // financier have permissions to withdraw ether from contract
    address public financier;

    // Allows to track whether contract is paused
    bool public isPaused = false;

    modifier onlyDeveloper() {
        require(msg.sender == developer);
        _;
    }

    modifier onlyFinancier() {
        require(msg.sender == financier);
        _;
    }

    modifier onlyAdmin() {
        require(msg.sender == developer || msg.sender == financier);
        _;
    }

    modifier whenPaused() {
        require(isPaused);
        _;
    }

    modifier whenNotPaused() {
        require(!isPaused);
        _;
    }

    function setDeveloper(address _newDeveloper) public onlyDeveloper {
        require(_newDeveloper != address(0));
        developer = _newDeveloper;
    }

    function setFinancier(address _newFinancier) public onlyFinancier {
        require(_newFinancier != address(0));
        financier = _newFinancier;
    }

    function withdrawBalance() external onlyFinancier {
        financier.transfer(this.balance);
    }

    // Called when bug was detected
    function pause() onlyAdmin public whenNotPaused {
        isPaused = true;
    }

    function resume() onlyAdmin public whenPaused {
        isPaused = false;
    }
}
