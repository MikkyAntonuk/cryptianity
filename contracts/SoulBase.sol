pragma solidity ^0.4.18;

import "./SoulsAccessControl.sol";

contract SoulBase is SoulsAccessControl{

    /*** EVENTS ***/

    // Calls when new soul was registered
    event Creation(address _address, uint256 _id);

    // Calls when soul was transferred from one account to another
    event Transfer(address _from, address _to, uint256 _id);

    /*** DATA TYPES ***/

    // Base structure for soul
    struct Soul {
        // Power of current soul. Changes when other souls upvote or downvote for its acts
        // according to the power game sorts souls
        uint256 soulPower;

        // There 3 types of status in game. Base soul - 0, Gold soul - 1, Platinum soul - 2
        uint8 soulStatus;

        // Link to the IPFS storage where stored all acts of soul
        // Example: QmPMi7nPnADyESdLwRuyAAiyZ85wu6BVW4EiYhNLgza3Ev
        bytes32 ipfsLink;
    }

    /*** STORAGE ***/

    // An array containing all existing souls. The index of every soul is index into this array
    Soul[] souls;

    // Mapping for soulsID and address what owns it
    mapping (uint256 => address) soulIndexToOwner;

    // Mapping for count how many souls has account
    // Analog to balanceOf in tokens
    mapping (address => uint256) soulTokenCount;

    /// A mapping from SoulID to an address that has been approved to call
    ///  transferFrom(). Each soul can only have one approved address for transfer
    ///  at any time. A zero value means no approval is outstanding.
    mapping (uint256 => address) soulIndexToApproved;

    /*** FUNCTIONS ***/

    // Assigns the specific soul to address
    function _transfer(address _from, address _to, uint256 _id) internal {
        soulTokenCount[_to]++;
        soulIndexToOwner[_id] = _to;

        // When creating new soul _from is 0x0, but we cannot account this address
        if (_from != address(0)) {
            soulTokenCount[_from]--;
            delete soulIndexToApproved[_id];
        }

        // Calling the Transfer event
        Transfer(_from, _to, _id);
    }

    // Internal function for creating new souls
    function _createSoul(address _owner, uint8 _status, uint256 _power, bytes32 _ipfsLink) internal returns (uint256){
        require(_status <= 2);
        require(_owner != 0);
        require(_power <= 4294967295);

        Soul memory _soul = Soul({
            soulPower: _power,
            soulStatus: _status,
            ipfsLink: _ipfsLink
        });

        uint256 id = souls.push(_soul) - 1;

        // Calling Creation event
        Creation(_owner, id);

        // Transfer ownership to owner
        _transfer(0, _owner, id);

        // Calling Transfer event
        Transfer(0, _owner, id);

        return id;
    }
}
