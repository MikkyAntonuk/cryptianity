const SoulToken = artifacts.require("ICO/SoulToken.sol");
const SoulCrowdsale = artifacts.require("ICO/SoulTokenCrowdsale.sol");

module.exports = function (deployer) {
    deployer.deploy(SoulToken);

    const start = web3.eth.getBlock(web3.eth.blockNumber).timestamp;;
    const end = start + 86400;
    const rate = 500;
    const wallet = 0x627306090abab3a6e1400e9345bc60c78a8bef57;
    deployer.deploy(SoulCrowdsale, start, end, rate, wallet);
};